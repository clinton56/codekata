package codekata.kata19;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@Test
public class WordsValidatorTest {

    private List<String> dictionaryStub = collectToList("cat", "car","cars", "cog", "cot", "dog", "elephant", "fog", "mug");
    private WordsValidator wordsValidator;

    @BeforeTest
    public void setUp() throws Exception {
        wordsValidator = new WordsValidator();
    }

    @DataProvider(name = "words")
    static Object[][] words() {
        return new Object[][]{{"cat", true}, {"blaaaa", false}, {"elephant", true}};
    }

    @Test(dataProvider = "words")
    public void testIfTheWordIsInDictionary(String word, boolean expectedResult) throws Exception {
        // given
        List<String> wordList = dictionaryStub;

        // when
        boolean actualResult = wordsValidator.isWordInDictionary(word, wordList);

        // then
        assertThat(actualResult, is(equalTo(expectedResult)));
    }

    @DataProvider(name = "wordPairs")
    static Object[][] wordPairs() {
        return new Object[][]{{"cat", "dog", true}, {"elephant", "pig", false}, {"tiger", "panda", true}};
    }

    @Test(dataProvider = "wordPairs")
    public void areTheWordsHavingSameLength(String firstWord, String secondWord, boolean expectedResult) {
        // given - when
        boolean actualResult = wordsValidator.checkHavingSameLength(firstWord, secondWord);

        // then
        assertThat(actualResult, is(equalTo(expectedResult)));
    }

    @DataProvider(name = "emptyWord")
    static Object[][] emptyWord() {
        return new Object[][]{{"cat", false}, {"", true}, {"elephant", false}};
    }

    @Test(dataProvider = "emptyWord")
    public void isWordEmpty(String word, boolean expectedResult) {
        // given - when
        boolean actualResult = wordsValidator.isEmpty(word);

        // then
        assertThat(actualResult, is(equalTo(expectedResult)));
    }

    @DataProvider(name = "wordValidate")
    static Object[][] wordValidate() {
        return new Object[][]{{"cat", "", Collections.emptyList(), false},
                {"cat", "car", collectToList("cat", "car"), true},
                {"", "cot", Collections.emptyList(), false},
                {"cat", "elephant", Collections.emptyList(), false},
                {"bla", "dog", Collections.emptyList(), false},
                {"cat", "yyy", Collections.emptyList(), false},
                {"cat", "cat", collectToList("cat"), true}};
    }

    @Test(dataProvider = "wordValidate")
    public void testWordsValidation(String firstWord, String secondWord, List<String> wordList, boolean expectedResult) {
        // given - when
        boolean actualResult = wordsValidator.validate(firstWord, secondWord, wordList);

        // then
        assertThat(actualResult, is(equalTo(expectedResult)));
    }

    private static List<String> collectToList(String... words) {
        return Stream.of(words).collect(Collectors.toList());
    }

}
