package codekata.kata19;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsNot.not;

@Test
public class WordChainsTest {

    private static final int WORD_LENGTH_OF_THREE = 3;
    private static final String DICTIONARY_FILE_NAME = "wordlist.txt";
    private WordChains wordChains;
    @Mock
    private WordsValidator validator;

    @BeforeTest
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        wordChains = new WordChains(validator);
    }

    public void testIfDictionaryIsNotEmpty() throws Exception {
        // given
        List<String> wordList;

        // when
        wordList = wordChains.readDictionaryFrom(DICTIONARY_FILE_NAME, WORD_LENGTH_OF_THREE);

        // then
        assertThat(wordList, is(not(empty())));
    }

    public void testFindingWordsWithTheSameLength() throws Exception {
        // given
        List<String> wordList;

        // when
        wordList = wordChains.readDictionaryFrom(DICTIONARY_FILE_NAME, WORD_LENGTH_OF_THREE);

        // then
        wordList.forEach(word -> assertThat(word.length(), is(equalTo(WORD_LENGTH_OF_THREE))));
    }

    @DataProvider(name = "wordPairsEquality")
    static Object[][] wordPairsEquality() {
        return new Object[][]{{"cat", "dog", false}, {"elephant", "pig", false}, {"dog", "dog", true}};
    }

    @Test(dataProvider = "wordPairsEquality")
    public void areTheWordsEqual(String firstWord, String secondWord, boolean expectedResult) {
        // given - when
        boolean actualResult = wordChains.areWordsEqual(firstWord, secondWord);

        // then
        assertThat(actualResult, is(equalTo(expectedResult)));
    }

    @DataProvider(name = "wordPairsAdjacent")
    static Object[][] wordPairsAdjacent() {
        return new Object[][]{{"cat", "dog", false}, {"cat", "car", true}, {"cat", "cot", true}, {"cat", "hat", true}};
    }

    @Test(dataProvider = "wordPairsAdjacent")
    public void testIfWordsDifferOnlyByOnePosition(String firstWord, String secondWord, boolean expectedResult) {
        // given - when
        boolean actualResult = wordChains.areWordsAdjacent(firstWord, secondWord);

        // then
        assertThat(actualResult, is(equalTo(expectedResult)));
    }

    @DataProvider(name = "wordChainBase")
    static Object[][] wordChainBase() {
        return new Object[][]{{"cat", "", Collections.emptyList(), false},
                {"cat", "car", collectToList("cat", "car"), true},
                {"", "cot", Collections.emptyList(), false},
                {"cat", "elephant", Collections.emptyList(), false},
                {"bla", "dog", Collections.emptyList(), false},
                {"cat", "yyy", Collections.emptyList(), false},
                {"cat", "cat", collectToList("cat"), true}};
    }

    @Test(dataProvider = "wordChainBase")
    public void testFindingChainBaseScenario(String startWord, String endWord, List<String> expectedChainOfWords, boolean isValid) throws Exception {
        // given
        List<String> wordList = wordChains.readDictionaryFrom(DICTIONARY_FILE_NAME, WORD_LENGTH_OF_THREE);

        // when
        Mockito.when(validator.validate(startWord, endWord, wordList)).thenReturn(isValid);
        List<String> actualChainOfWords = wordChains.findChain(startWord, endWord, wordList);

        // then
        assertThat(actualChainOfWords.containsAll(expectedChainOfWords), is(true));
    }

    public void testGettingAllAdjacentWords() throws Exception {
        // given
        String word = "cat";
        List<String> wordList = wordChains.readDictionaryFrom(DICTIONARY_FILE_NAME, WORD_LENGTH_OF_THREE);
        List<String> expectedAdjacentWords = collectToList("car", "cot");

        // when
        List<String> adjacentWords = wordChains.getAllAdjacentWordsTo(word, wordList);

        // then
        assertThat(adjacentWords.containsAll(expectedAdjacentWords), is(true));
    }

    public void testGettingPathFromVisitedWords() {
        // given
        String startWord = "cat";
        String endWord = "dog";
        Map<String, String> visitedWordsMap = new HashMap<>();
        visitedWordsMap.put("cot", "cat");
        visitedWordsMap.put("cog", "cot");
        visitedWordsMap.put("dog", "cog");
        List<String> expectedPath = collectToList("cat", "cot", "cog", "dog");

        // when
        List<String> actualWordPath = wordChains.getPathFrom(startWord, endWord, visitedWordsMap);

        // then
        assertThat(actualWordPath.containsAll(expectedPath), is(true));
    }

    @DataProvider(name = "wordsChainComplex")
    static Object[][] wordsChainComplex() {
        return new Object[][]{{"cat", "dog", collectToList("cat", "cot", "cog", "dog"), 3},
                {"lead", "gold", collectToList("lead", "load", "goad", "gold"), 4},
                {"gold", "lead", collectToList("gold", "goad", "load", "lead"), 4},
                {"the", "end", collectToList("the", "che", "chs", "ehs", "ens", "end"), 3},
                {"brushing", "cheating", collectToList("brushing", "crushing", "crusting", "cresting", "creating", "cheating"), 8},
        };
    }

    @Test(dataProvider = "wordsChainComplex")
    public void testGettingShortestChainOfWords(String startWord, String endWord, List<String> expectedWordsChain,
                                                int wordLength) throws Exception {
        // given
        List<String> wordList = wordChains.readDictionaryFrom(DICTIONARY_FILE_NAME, wordLength);

        // when
        List<String> actualWordsChain = wordChains.findShortestChain(startWord, endWord, wordList);

        // then
        assertThat(actualWordsChain.containsAll(expectedWordsChain), is(true));
    }

    private static List<String> collectToList(String... words) {
        return Stream.of(words)
                .collect(Collectors.toList());
    }

}
