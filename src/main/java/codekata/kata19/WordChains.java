package codekata.kata19;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class WordChains {

    private WordsValidator validator;

    WordChains(WordsValidator validator) {
        this.validator = validator;
    }

    List<String> readDictionaryFrom(String dictionaryFileName, int wordLength) throws IOException, URISyntaxException {
        Path path = Paths.get(ClassLoader.getSystemResource(dictionaryFileName).toURI());
        try(Stream<String> stream = Files.lines(path)) {
            return stream.filter(word -> word.length() == wordLength)
                    .collect(Collectors.toList());
        }
    }

    boolean areWordsEqual(String firstWord, String secondWord) {
        return Objects.equals(firstWord, secondWord);
    }

    boolean areWordsAdjacent(String firstWord, String secondWord) {
        int numberOfDifferentChars = 0;
        final int ONLY_ONE = 1;
        for (int i = 0; i < firstWord.length(); i++) {
            if (firstWord.charAt(i) != secondWord.charAt(i)) {
                numberOfDifferentChars++;
            }
        }
        return numberOfDifferentChars == ONLY_ONE;
    }

    List<String> findChain(String startWord, String endWord, List<String> wordList) {
        if (validator.validate(startWord, endWord, wordList)) {
            if (areWordsEqual(startWord, endWord) || areWordsAdjacent(startWord, endWord)) {
                return collectToList(startWord, endWord);
            }
            return findShortestChain(startWord, endWord, wordList);
        }
        return Collections.emptyList();

    }

    List<String> getAllAdjacentWordsTo(String word, List<String> wordList) {
        return wordList.stream()
                .filter(w -> areWordsAdjacent(word, w))
                .collect(Collectors.toList());
    }

    List<String> getPathFrom(String startWord, String endWord, Map<String, String> visitedWordsMap) {
        if (!visitedWordsMap.containsKey(endWord)) {
            return Collections.emptyList();
        }
        String word = endWord;
        List<String> result = new ArrayList<>();
        do {
            result.add(0, word);
            word = visitedWordsMap.get(word);
        } while (!word.equals(startWord));
        result.add(0, startWord);
        return result;
    }

    private List<String> collectToList(String... words) {
        return Stream.of(words)
                .collect(Collectors.toList());
    }

    List<String> findShortestChain(String startWord, String endWord, List<String> wordList) {

        String currentWord;
        Queue<String> wordsQueue = new LinkedList<>();
        wordsQueue.add(startWord);
        Map<String, String> visitedWords = new HashMap<>();

        while (!wordsQueue.isEmpty() && !(currentWord = wordsQueue.remove()).equals(endWord)) {
            List<String> adjacentWords = getAllAdjacentWordsTo(currentWord, wordList);
            for (String nextWord : adjacentWords) {
                if (!visitedWords.containsKey(nextWord)) {
                    visitedWords.put(nextWord, currentWord);
                    wordsQueue.add(nextWord);
                }
            }

        }
        return getPathFrom(startWord, endWord, visitedWords);
    }

    public static void main(String[] args) throws Exception {
        WordsValidator validator = new WordsValidator();
        WordChains wordChains = new WordChains(validator);

        String startWord = "cat";
        String endWord = "dog";
        List<String> dictionary = wordChains.readDictionaryFrom("wordlist.txt", startWord.length());
        System.out.println(wordChains.findChain(startWord, endWord, dictionary));

    }

}
