package codekata.kata19;

import java.util.List;

class WordsValidator {

    boolean validate(String word1, String word2, List<String> wordList) {
        return isWordInDictionary(word1, wordList) && isWordInDictionary(word2, wordList)
                && checkHavingSameLength(word1, word2) && !isEmpty(word1) && !isEmpty(word2);
    }

    boolean isWordInDictionary(String word, List<String> wordList) {
        return wordList.contains(word);
    }

    boolean checkHavingSameLength(String firstWord, String secondWord) {
        return firstWord.length() == secondWord.length();
    }

    boolean isEmpty(String word) {
        return word.isEmpty();
    }
}
